<?php 
require_once 'header.php';


$products = $db->query('SELECT * FROM products ORDER BY date DESC LIMIT 6')->fetchAll();

$rand_products = $db->query('SELECT * FROM products WHERE rating BETWEEN 3 AND 5 ORDER BY RAND() LIMIT 2')->fetchAll();

?>
        <div class="row">

            <div class="col-md-3">

                <p class="lead">Categories</p>
                <div class="list-group">
                    <a href="#" class="list-group-item">Category 1</a>
                    <a href="#" class="list-group-item">Category 2</a>
                    <a href="#" class="list-group-item">Category 3</a>
                </div>

                <!-- FEATURED PRODUCTS RANDOM -->
                <p class="lead">Featured products</p>

                <?php 

                    foreach ($rand_products as $key => $rand_product) {

                ?>
                <div class="product">
                    <div class="thumbnail">
                        <img src="img/product/<?= $rand_product['picture'] ?>" alt="">
                        <div class="caption">
                            <h4 class="pull-right"><?= $rand_product['price'] ?> &euro;</h4>
                            <h4><a href="#"><?= $rand_product['name'] ?></a>
                            </h4>
                            <p><?= cutText($rand_product['description'], 100) ?></p>
                        </div>
                        <div class="ratings">
                            <p class="pull-right">12 reviews</p>
                            <p>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star-empty"></span>
                            </p>
                        </div>
                        <div class="btns clearfix">
                            <a class="btn btn-info pull-left" href="product.php?id=<?= $rand_product['id'] ?>"><span class="glyphicon glyphicon-eye-open"></span> View</a>
                            <a class="btn btn-primary pull-right" href=""><span class="glyphicon glyphicon-shopping-cart"></span> Add to cart</a>
                        </div>
                    </div><!-- /.thumbnail -->
                </div><!-- /.product -->

                <?php } ?>

            </div><!-- /.col-md-3 -->
            <!-- END FEATURED PRODUCTS RANDOM -->

            <div class="col-md-9">

                <div class="row carousel-holder">

                    <div class="col-md-12">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="4"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img class="slide-image" src="img/slider/slider1.jpg" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="img/slider/slider2.jpg" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="img/slider/slider3.jpg" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="img/slider/slider4.jpg" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="img/slider/slider5.jpg" alt="">
                                </div>
                            </div>
                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div><!-- /.carousel -->
                    </div><!-- /.col-md-12 -->

                </div><!-- /.row.carousel-holder -->

                <div class="row">
                    <?php

                        foreach ($products as $key => $product) {

                    ?>
                    <div class="product col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="img/product/<?= $product['picture'] ?>" alt="<?= $product['name'] ?>">
                            <div class="caption">
                                <h4 class="pull-right"><?= $product['price'] ?> &euro;</h4>
                                <h4><a href="#"><?= $product['name'] ?></a>
                                </h4>
                                <p><?= cutText($product['description'], 100) ?></p>
                            </div>
                            <div class="ratings">
                                <p class="pull-right">15 reviews</p>
                                <p>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                </p>
                            </div>
                            <div class="btns clearfix">
                                <a class="btn btn-info pull-left" href="product.php?id=<?= $product['id'] ?>"><span class="glyphicon glyphicon-eye-open"></span> View</a>
                                <a class="btn btn-primary pull-right" href="product.php"><span class="glyphicon glyphicon-shopping-cart"></span> Add to cart</a>
                            </div>
                        </div><!-- /.thumbnail -->
                    </div><!-- /.product -->
                   <?php } ?>

                   
                </div><!-- /.row -->

            </div><!-- col-md-9 -->

        </div><!-- /.row -->

    <?php 
    require_once 'footer.php';
    ?>