<?php 

function cutText($text, $max_length = 0, $end = '...', $sep = '[@]') {

	// max_length = 0 - minimum de caractères à avoir est de 0
	if($max_length > 0 && strlen($text) > $max_length) {
		$text = wordwrap($text, $max_length, $sep);
		$text = explode($sep, $text);

		return $text[0].$end;
	}

	return $text;
}
