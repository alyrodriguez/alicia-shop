<?php
define('HOST', 'localhost');
define('USER', 'root');
define('PASS', '');
define('DB', 'shop');   

// Gestion des erreurs de connexion à MySQL:
try {
    global $db;
    $db_options = array(
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING
    );

// Connexion à la base de données - db: data base:
    $db = new PDO('mysql:host='.HOST.';dbname='.DB.'', USER, PASS, $db_options);

// Si il y a la moindre erreur dans les lignes plus haut le serveur va se déconnecter automatiquement grâce aux lignes ci-dessous:
} catch (Exception $e) {
    exit('MySQL Connect Error >> '.$e->getMessage());
}
