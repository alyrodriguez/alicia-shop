<?php
require_once 'header.php';

// $categories = $db->query('SELECT category FROM products DESC')->fetchAll();

$count_results = 0;
 $search_results = array();
?>

        <div class="row">
            <div class="col-lg-12">

                <h1 class="page-header">Search</h1>

                <form class="search form-inline" method="GET">
                    <div class="form-group">
                        <input type="text" id="keywords" name="keywords" class="form-control" placeholder="Keywords" value="">
                    </div>

                    <div class="form-group">
                        <select id="category" name="category" class="form-control">
                            <option value="">...</option>
                            <option value="">1</option>
                            <option value="">2</option>
                            <option value="">3</option>

                        </select>
                    </div>



                    <div class="form-group">
                        <label for="price">Price</label>
                        0 € <input id="price" name="price" type="text" value="" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="[0,100]"/> 100 €
                    </div>

                    <div class="form-group">
                        <label class="checkbox-inline">
                            <input type="checkbox" id="picture" name="picture" value="1"> Picture
                        </label>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-default">
                            <span class="glyphicon glyphicon-search" aria-hidden="true"></span> Search
                        </button>
                    </div>
                </form>

            </div><!-- /.col-lg-12 -->
        </div><!-- /.row -->


<?php

    if(!empty($_GET)) {

        $count_results = 0;
        $search_results = array();

        if(!empty($_GET['search'])){

            $search = $_GET['search'];

            $filter = ' WHERE name LIKE :search OR description LIKE :search OR category LIKE :search';

            $query = $db->prepare('SELECT COUNT(*) as count_total FROM products'.$filter);
            $query->bindValue('search', '%'.$search.'%');
            $query->execute();
            $result = $query->fetch();
            $count_results = $result['count_total'];

            $query = $db->prepare('SELECT * FROM products '.$filter);
            $query->bindValue('search', '%'.$search.'%');
            $query->execute();
            $search_results = $query->fetchAll();
        }
    } else {

        $bindings = array();

        $select_count = 'SELECT COUNT(*) as count_total ';
        $select = 'SELECT * ';

        $from = ' FROM products ';
        $where = ' WHERE 1 ';


        if(!empty($name))  {
            $where .= ' AND name LIKE :name';
            $bindings['name'] = '%'.$name.'%';
        }

        if(!empty($description)) {
            $where .= ' AND description LIKE :description';
            $bindings['description'] = '%'.$description.'%';
        }

        // if(!empty($category)) {
        //     $where .= ' AND category LIKE :category';
        //     $bindings['category'] = '%'.$category.'%';
        // }

        if(!empty($price)) {
            $where .= ' AND price = :price';
            $bindings['price'] = $price;
        }

        // Pour que quand les champs sont vides il n'aille pas chercher tous les films dans la BDD --> affiche 0 résultats
        if(!empty($bindings)) {

            $sql_count = $select_count.$from.$where;
            $sql = $select.$from.$where;

            $query = $db->prepare($sql_count);
            foreach ($bindings as $key => $binding) {
                $query->bindValue($key, $binding);

            }
            $query->execute();
            $result = $query->fetch();
            $count_results = $result['count_total'];
            // le count_total c'est pour le nombre de films correspondants trouvés lors de la recherche

            // $query = $db->prepare($sql);
            // $bindings['start'] = ($page - 1) * $nb_items_per_page;
            // $bindings['nb_items'] = $nb_items_per_page;

            foreach ($bindings as $key => $value) {
                $type = is_int($value) ? PDO::PARAM_INT : PDO::PARAM_STR;
                $query->bindValue($key, $value, $type);
            }
            $query->execute();
            $search_results = $query->fetchAll();
        }
        // debug($bindings);
    }


?>


        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><?= $count_results ?> search results for <?= !empty($search) ? '"'.$search.'"' : ''?></h1>
            </div><!-- /.col-lg-12 -->
        </div><!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="pull-right">
                    <form class="form-inline" method="GET">
                        <div class="form-group">
                            <label for="sort">Sort by</label>
                            <select id="sort" name="sort" class="form-control">
                                <option value="name">Name</option>
                                <option value="price">Price</option>
                                <option value="rating">Rating</option>
                                <option value="reviews">Reviews</option>
                            </select>
                            <select id="direction" name="direction" class="form-control">
                                <option value="ASC">Ascending</option>
                                <option value="DESC">Descending</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-default">
                                <span class="glyphicon glyphicon-sort" aria-hidden="true"></span>
                            </button>
                        </div>
                    </form>
                </div>

            </div><!-- /.col-lg-12 -->
        </div><!-- /.row -->

        <hr>

        <div class="row">
            <div class="col-lg-12">

            <?php 

                foreach ($search_results as $product) {

            ?>
                <div class="product col-lg-3 col-md-4 col-xs-6 thumb">
                    <div class="thumbnail">
                        <img src="img/product/<?= $product['picture'] ?>" alt="">
                        <div class="caption">
                            <h4 class="pull-right"><?= $product['price']?> &euro;</h4>
                            <h4><a href="#"><?= $product['name']?></a>
                            </h4>
                            <p><?= cutText($product['description'], 100) ?></p>
                        </div>
                        <div class="ratings">
                            <p class="pull-right">12 reviews</p>
                            <p>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star-empty"></span>
                            </p>
                        </div>
                        <div class="btns clearfix">
                            <a class="btn btn-info pull-left" href="product.php?id=<?= $rand_product['id'] ?>"><span class="glyphicon glyphicon-eye-open"></span> View</a>
                            <a class="btn btn-primary pull-right" href=""><span class="glyphicon glyphicon-shopping-cart"></span> Add to cart</a>
                        </div>
                    </div><!-- /.thumbnail -->
                </div><!-- /.product -->

               <?php } ?>

            </div><!-- /.col-lg-12 -->
        </div><!-- /.row -->

<?php
require_once 'footer.php';
?>